# 100 Days Of DevOps - Log

### Day 16: May 17 2021

**Today's Progress**:

- Adding workwell app in AWS device farm for testing
- Trying AWS farm' fuzzy test on workwell. 

### Day 15: May 12 2021

**Today's Progress**:

- Setting up AWS FARM for testing. 
- getting around AWS farm console 


### Day 14: May 10 2021

**Today's Progress**:

- Started looking up for AWS Device Farm
- Going through official blogs/videos about AWS farm


### Day 13: May 7 2021

**Today's Progress**:

- Functions in Terraform
- Conditional Expressions 
- Hands on using fuctions and conditional expressions
- Workspaces in TF
- Making use of workspace in kodecloud's lab. 

**Thoughts:** 
- This course came to end today, and I strongly think that if anyone is getting their hands on TF, this helps to know the TF platform smoothly.

### Day 12: May 6 2021

**Today's Progress**:

- What are modules?
- Creating local module
- Using remote modules from TF registry
- Hands on labs using modules locally and remotly


### Day 11: May 5 2021

**Today's Progress**:

- TF taint / untaint cmd 
- Debugging in TF with KodeKloud's lab
- TF importing mechanism 
- hands on TF importing resources 


### Day 10: May 4 2021

**Today's Progress**:

- AWS EC2 services, how they work?
- EC2 Configurations with AWS console
- Creating EC2 instance in AWS console and all the mandatory configs
- Creating EC2 instance with terraform with all the configs
- What are TF provisioners?
- AWS EC2 - TF provisioners 
- Hands on labs with AWS EC2 and TF provisioners

### Day 9: May 3 2021

**Today's Progress**:

- What is DynamoDB and how it works
- Creating resource for DynamoDB with TF configs
- Remote State Locking
- Remote Backend with S3
- Terraform State commands for handling state 

### Day 8: April 30 2021

**Today's Progress**:

- Hands on IAM services in TF 
- Creating and destructing resources for IAM services and its policies
- Getting hands on AWS S3 with TF

### Day 7: April 29 2021

**Today's Progress**:

- Setting up AWS console
- Creating users in AWS and configuring rules with GUI
- Creating users in AWS and configuring rules with AWS CLI
- AWS IAM and IAM policies via CLI configs


### Day 6: April 28 2021

**Today's Progress**:

- Meta Arguments and how to make use of them in TF
- Hands on Functions such as Count / for_each / each 
- Version constraints, how to use specific version, how to install or condition for specific version. 
- Starting with AWS Demo Setup

### Day 5: April 27 2021

**Today's Progress**:

- Handling state in terraform / Why we need state?
- How does state work and the locking mechanism
- Mutable and immutable infrastructure 
- Lifecycle rules and how-when to make use of them
- how to handle Datasources in terraform?

**Thoughts:**

- making state locked when it's in use is something that I loved about so that there as there is always is gonna be one single source of truth.
- Didn't thought of lifecycle would be there for terraform but now it feels like so much can be done. Specially about `create_before_destroy`


### Day 4: April 26 2021

**Today's Progress**:

- How to use terraform variables
- Resource dependencies-How does terraform resources execution works 
- making use of output variables
- using output of one resource into another 
- how to access output variables



### Day 3: April 23 2021

**Today's Progress**:

- Understanding creating single providers and multiple providers.
- How resources work and try creating providers and providers resources, using "local_file" resource.
- Hands-on creating and updating , destroying the provider.


### Day 2: April 22 2021

**Today's Progress**:

- Started with KodeCloud's Beginner's guide to Terraform
- Going through Hashicorp Configuration Language. (HCL)
- Hands on HCL concepts and terraform basic configurations 

**Thoughts:** 

- -

### Day 1: April 21 2021

**Today's Progress**:

- Started with John Willis's chaper 3
- Understanding Value stream mapping and it's use cases in real world. 
- How Waste Analysis is important and can be performed.
- Improvement Paradox where you need to look into the risk of the factor and the rewards that you gonna get out of.
- Understanding the organizational change subcategoried into Individual and Team changes. 

**Thoughts:** 

- Waste Analysis was an amazing thing that caught my eye about how things could be better in process.
- To help team and individual grow, understanding organizational change is important and how each personnel can make best out of their own within. 


### Day 0: April 20 2021

**Today's Progress**:

- Started with KodeKloud's prerequisite to Devops.
- Getting hands on exp on KodeKloud's labs.
- Started with John Willis's introduction to devops.
- Majorly trying to understand "Three ways to the Devops" reading more about it to the blogs videos.
- Came across "Chaos Engg" and netflix's approach to it.

**Thoughts:** 
I think chaos engg is something that we should look into. 
Really loved the real use case of Netflix's chaos monkey. Fav Quote: "It's better to have minor bugs than getting  bigger one unexpectedly." 



