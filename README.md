# Particle41's 100DaysOfDevOps Challenge

This is a stripped down fork of the [#100DaysOfCode](https://github.com/kallaway/100-days-of-code) challenge, for internal use at Particle41.

The rules are a little more flexible and adapted for our usage.

It is compatible with the full challenge, so you can do that instead if you prefer.

Check out [the Official Site](http://100daysofcode.com/) and the GitHub [repo](https://github.com/kallaway/100-days-of-code) for the #100DaysOfCode movement.

## Rules

The main commitment:

*I will code for at least an hour every day for the next 100 days, working on DevOps projects.*

Additional rules:

- I will post updates about my progress every day (we are doing this on our Slack)
- I will update the [Log](log.md) with the day's progress and provide a link so that others can see my progress
- I will work on real projects, facing real challenges.

## Get started

0. Fork this repo and commit to the [Log](log.md).

1. Delete the examples in the log, or comment them out, and start filling it with your own content.

2. Encourage others who are doing the same challenge - by giving them props when they are posting updates on their progress, supporting them when things get difficult!

3. If you find a great, helpful resource that others would benefit from, add it to our Resources page in Confluence:

<https://particle41.atlassian.net/wiki/spaces/DEVOPS/pages/951123969/DevOps+Resources>

## Contents

* [Log](log.md)
